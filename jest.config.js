/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

module.exports = {
  // All imported modules in your tests should be mocked automatically
  // automock: false,


  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  // An array of regexp pattern strings used to skip coverage collection
  coveragePathIgnorePatterns: [
    "/node_modules/"
  ],

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "v8",

  globalSetup: '<rootDir>/src/__tests__/setup.js',
  globalTeardown: '<rootDir>/src/__tests__/teardown.js',
  setupFilesAfterEnv: ['<rootDir>/src/__tests__/setupAfterEnv.js'],
  testEnvironment: '<rootDir>/src/__tests__/environment.js',
  testMatch: ['**/__tests__/**/*.test.js']

};
