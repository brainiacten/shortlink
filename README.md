# ShortLink

ShortLink is a URL shortening service where you enter a URL such as https://indicina.co and it
returns a short URL such as http://short.est/GeAi9K. Visiting the shortened URL should redirect
the user to the long URL. Using the example above, visiting


# clone the repo
git clone https://gitlab.com/brainiacten/shortlink.git

cd shortlink

# Run with docker 
`npm start`

# Run directly
### Install dependencies
`npm install`

### Set up Environment
create `.env` file
Copy the content of `env.sample` and paste into `.env` file

or

rename `env.sample` to `.env`

### Run project
Run `npm run dev` to start the project

Run `npm run test` to run test

### Project Details
1. The project is currently defined to run on port 8000 but this can be changed in the`.env` file
2. http://localhost:8000/encode is the endpoint used to encode an url. The params are
  ` {
   "shortCode": "GeAi9K",
   "url": "https://www.indicina.co/"
   }`
   if `shortCode` is not passed in, it is automatically generated
   
3. http://localhost:8000/GeAi9K is the endpoint used to decode a short link, where `GeAi9K` is the `shortCode`
4. http://localhost:8000/statistics/I8Am39 is the endpoint used to retrieve statistics about a link, where I8Am39 is the `shortCode` returned during encoding 

