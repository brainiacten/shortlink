const supertest = require('supertest')
const { encode } = require('../testData/encode')
const app = require('../../app')
const request = supertest(app)

describe('Encode Test', () => {
  it('should encode a url using the short code passed in', async () => {
    const res = await request.post('/encode').send(encode).expect('Content-Type', /json/)
    expect(res.statusCode).toEqual(200)
    expect(res.body.message).toEqual('Short Link generated successfully!')
    expect(Object.keys(res.body.data)).toEqual(expect.objectContaining(['origUrl', 'shortUrl', 'shortCode']))
  })
})
