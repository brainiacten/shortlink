const supertest = require('supertest')
const { shortCode } = require('../testData/decode')
const app = require('../../app')
const request = supertest(app)
const URL = require('../../models/url.models')
const { encode } = require('../testData/encode')
const Helper = require('../../config/helper')

describe('Decode Test', () => {
  it('should redirect to original url', async () => {
    const baseUrl = Helper.baseURL
    const shortUrl = baseUrl + encode.shortCode
    await URL.create({ origUrl: encode.url, shortCode: encode.shortCode, shortUrl })

    const res = await request.get(`/${shortCode}`)
    expect(res.statusCode).toEqual(302)
    expect(res.header.location).toEqual(encode.url)
  })
})
