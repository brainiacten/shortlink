const supertest = require('supertest')
const { shortCode } = require('../testData/decode')
const app = require('../../app')
const request = supertest(app)
const URL = require('../../models/url.models')
const { encode } = require('../testData/encode')
const Helper = require('../../config/helper')

describe('Statistics Test', () => {
  it('should return the statistics for a short url', async () => {
    const baseUrl = Helper.baseURL
    const shortUrl = baseUrl + encode.shortCode
    await URL.create({ origUrl: encode.url, shortCode: encode.shortCode, shortUrl })

    const res = await request.get(`/statistics/${shortCode}`)
    expect(res.statusCode).toEqual(200)
    expect(res.body.message).toEqual('Short Link statistics retrieved successfully!')
    expect(Object.keys(res.body.data)).toEqual(expect.objectContaining(['visits', 'visitingIp', 'lastVisited']))
  })
})
