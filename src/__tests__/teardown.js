const MemoryDatabaseServer = require('../config/memoryDatabaseServer')

module.exports = async () => {
  await MemoryDatabaseServer.stop()
}
