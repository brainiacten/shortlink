const Url = require('../models/url.models')
const Helper = require('../config/helper')
const randomstring = require('randomstring')
const validUrl = require('valid-url')
const RequestIp = require('@supercharge/request-ip')

exports.encode = async (req, res) => {
  if (validUrl.isUri(req.body.url)) {
    try {
      const baseUrl = Helper.baseURL
      const origUrl = req.body.url
      const shortCode = req.body.shortCode || randomstring.generate(6)
      const shortUrl = baseUrl + shortCode

      const urlExists = await Url.findOne({ origUrl })
      if (urlExists) {
        return res.status(200).json(urlExists)
      } else {
        const newUrl = await Url.create({
          origUrl,
          shortUrl,
          shortCode
        })
        return res.status(200).json({ data: newUrl ? { origUrl: newUrl.origUrl, shortUrl: newUrl.shortUrl, shortCode: newUrl.shortCode } : {}, message: 'Short Link generated successfully!' })
      }
    } catch (err) {
      res.status(500).json('Server Error')
    }
  } else {
    res.status(400).json('Invalid URL')
  }
}

exports.decode = async (req, res) => {
  let ip = RequestIp.getClientIp(req)
  // Convert IPv6 loopback address
  if (ip === '::1') {
    ip = 'http://127.0.0.1'
  }
  try {
    const urlObject = await Url.findOne({ shortCode: req.params.shortCode })
    if (urlObject) {
      let visits = urlObject.visits
      visits++
      await urlObject.updateOne({ visits, $addToSet: { visitingIp: ip } })
      return res.redirect(urlObject.origUrl)
    } else {
      return res.status(404).json('No URL Found')
    }
  } catch (err) {
    res.status(500).json('Server Error')
  }
}

exports.statistics = async (req, res) => {
  try {
    const urlExists = await Url.findOne({ shortCode: req.params.shortCode }).select('-_id visitingIp visits updatedAt').lean().exec()
    if (urlExists) {
      return res.status(200).json({
        data: {
          visits: urlExists.visits,
          visitingIp: urlExists.visitingIp,
          lastVisited: urlExists.updatedAt
        },
        message: 'Short Link statistics retrieved successfully!'
      })
    } else {
      return res.status(400).json('Url not found')
    }
  } catch (err) {
    res.status(500).json('Server Error')
  }
}
