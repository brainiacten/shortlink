// 'use strict'
//
// const config = require('./helper')
// const mongoose = require('mongoose')
//
// mongoose.Promise = require('bluebird')
// mongoose.connection.on('connected', () => {
//   console.log(`SHORT-LINK-DB is Ready!`)
// })
//
// mongoose.connection.on('disconnected', (err) => {
//   console.warn(`SHORT-LINK-DB disconnect from MongoDB via Mongoose because of ${err}`)
// })
//
// mongoose.connection.on('error', (err) => {
//   console.warn(`Could not connect to SHORT-LINK-DB because of ${err}`)
//   process.exit(-1)
// })
//
// exports.connect = () => {
//   const mongoURI = config.dbConfig.mongoUrl
//   return new Promise((resolve, reject) => {
//     mongoose
//       .connect(mongoURI, {
//         useNewUrlParser: true,
//         maxPoolSize: 2,
//         socketTimeoutMS: 600000
//       })
//       .then(() => {
//         resolve(mongoose.connection)
//       })
//       .catch((err) => reject(err))
//   })
// }

const mongoose = require('mongoose')

const connect = async () => {
  if (mongoose.connection.readyState === 0) {
    await mongoose.connect(process.env.NODE_ENV === 'test' ? global.__DB_URL__ : process.env.DB_URL, {
      useNewUrlParser: true,
      maxPoolSize: 2,
      socketTimeoutMS: 600000
    })
  }
}

const truncate = async () => {
  if (mongoose.connection.readyState !== 0) {
    const { collections } = mongoose.connection

    const promises = Object.keys(collections).map((collection) => mongoose.connection.collection(collection).deleteMany({}))

    await Promise.all(promises)
  }
}

const disconnect = async () => {
  if (mongoose.connection.readyState !== 0) {
    await mongoose.disconnect()
  }
}

mongoose.connection.on('connected', () => {
  console.log(`Short-Link-DB Ready!`)
})

mongoose.connection.on('disconnected', (err) => {
  console.log(`Short-Link-DB disconnect from MongoDB via Mongoose because of ${err}`)
})

module.exports = {
  connect,
  truncate,
  disconnect
}
