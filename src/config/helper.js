const env = process.env.NODE_ENV

const dbConfig = {
  mongoUrl: process.env.DB_URL
}

const port = process.env.PORT

const baseURL = process.env.baseURL

module.exports = { env, dbConfig, port, baseURL }
