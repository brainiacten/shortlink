const express = require('express')

const UrlController = require('../controller/url.controller')
const validation = require('../middlewares/validation')
const { encode, decode, statistics } = require('../validation/url.validation')
const routes = new express.Router()

routes.post('/encode', validation(encode), UrlController.encode)
routes.get('/:shortCode', UrlController.decode)
routes.get('/statistics/:shortCode', UrlController.statistics)

module.exports = routes
