'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const randomstring = require('randomstring')

const urlSchema = new Schema(
  {
    origUrl: {
      type: String,
      required: true
    },
    shortUrl: {
      type: String,
      required: true
    },
    visits: {
      type: Number,
      required: true,
      default: 0
    },
    visitingIp: [
      {
        type: String
      }
    ],
    shortCode: {
      type: String,
      required: true,
      default: randomstring.generate(6)
    }
  },
  {
    timestamps: true
  }
)

urlSchema.method({})

urlSchema.statics = {}

module.exports = mongoose.model('Url', urlSchema)
