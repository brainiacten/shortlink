require('dotenv').config()

const express = require('express')

const routes = require('./routes')

const DB = require('./config/db')

class App {
  constructor() {
    this.express = express()

    this.database()
    this.middlewares()
    this.routes()
  }

  database() {
    DB.connect()
  }

  middlewares() {
    this.express.use(express.json())
  }

  routes() {
    this.express.use(routes)
  }
}

module.exports = new App().express
