const Joi = require('joi')

const schemas = {
  encode: Joi.object().keys({
    url: Joi.string().required(),
    shortCode: Joi.string().optional()
  }),
  decode: Joi.object().keys({
    shortCode: Joi.string().required()
  }),
  statistics: Joi.object().keys({
    shortCode: Joi.string().required()
  })
}

module.exports = schemas
