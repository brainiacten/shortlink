const app = require('./app')
const Helper = require('./config/helper')
const bodyParser = require('body-parser')

const PORT = Helper.port
app.use(bodyParser.json())
app.listen(PORT, () => console.log('Server is listening on port ' + PORT))
