FROM node:14

WORKDIR /usr/src/short-link

COPY ./package.json ./

RUN npm install

COPY ./ ./

EXPOSE 8000

CMD [ "npm", "run", "dev" ]